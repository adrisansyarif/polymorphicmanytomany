<?php

use Illuminate\Support\Facades\Route;
use App\Models\Video;
use App\Models\Post;
use App\Models\Tag;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/create', function() {
    $video = Video::create(['name'=>'my first video.mkv']);

    $tag2 = Tag::findOrFail(2);
    $video->tags()->save($tag2);

    $post = Post::create(['name'=>'my first post']);

    $tag1 = Tag::findOrFail(1);
    $post->tags()->save($tag1);

  
});


Route::get('/read', function() {

    $post = Post::find(6);

    foreach($post->tags as $tag) {

        echo $tag;
    }

});

Route::get('/update', function() {

    // $post = Post::find(6);

    // foreach($post->tags as $tag) {

    //     return $tag->whereName('JAVA')->update(['name' =>'Pythom']);

        
    // }
    

    $post = Post::find(6);

    $tag = Tag::find(2);

    // $post->tags()->save($tag);

    // $post->tags()->attach($tag);

    $post->tags()->sync($tag);

});

Route::get('/delete', function() {

    $post = Post::find(1);

    foreach($post->tags as $tag) {

        $tag->whereId(2)->delete();
        
    }

});